public class Main {

    public static void main(String[] args) {
        //1 задача
        Operator operator = new Operator();
        String r1 = operator.op(22, 33);
        System.out.println("1-е задание = " + r1);

        //2 задача
        Chetnost chetnost = new Chetnost();
        int r2 = chetnost.chet(21, 3);
        System.out.println("2-е задание = " + r2);

        //3 задача
        Zadacha3 zadacha3 = new Zadacha3();
        int r3 = zadacha3.zad3(1, -3, 2);
        System.out.println("3-е задание = " + r3);

        //4 задача
        Zadacha4 zadacha4 = new Zadacha4();
        int r4 = zadacha4.zad4(1, 1, 1);
        System.out.println("4-е задание = " + r4);

        //5 задача
        Zadacha5 zadacha5 = new Zadacha5();
        String r5 = zadacha5.zad5(888);
        System.out.println("5-е задание = Оценка студента - " + r5);

    }
}
